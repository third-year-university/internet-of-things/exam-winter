#include "WIFI.h"
#include "Server.h"
#include "MQTT.h"
#include "Sensor_light.h"
#include <SoftwareSerial.h>

#define LED_PIN 15

SoftwareSerial ss(13, 12);
int BOARD = 1;

void setup() {
  Serial.begin(9600);
  ss.begin(9600);
  
  WiFi_init(false);
  
  MQTT_init();
  init_light_sensor();
  sensor_init();
  pinMode(LED_PIN, OUTPUT);
  
  if (BOARD == 1){
    pinMode(LED_BUILTIN, OUTPUT);
    server_init();
    mqtt_cli.subscribe(String("esp8266696D/sensor_light").c_str());
    mqtt_cli.subscribe(String("esp8266696D/sensor_distance").c_str());
  }
  else if (BOARD ==2){
  }
}

void loop() {

  if (BOARD == 1){
    server.handleClient();
    mqtt_cli.loop();
    if (sensor_to_use == 1){
      ss.write('L');
      ss.write(get_light_sensor_value());
    }
    else if (sensor_to_use == 0){
      ss.write('D');
      ss.write(min(int(sensor_get_dist()), 255));
    }
    delay(1000);
  }
  else if (BOARD == 2){
    if (ss.available() >= 2){
      char c = ss.read();
      int value = ss.read();
      ss.flush();
      if (c == 'L'){
        mqtt_cli.publish((mqtt_client_id + "/sensor_light").c_str(), String(value).c_str());
      }
      else if (c == 'D'){
        mqtt_cli.publish((mqtt_client_id + "/sensor_distance").c_str(), String(value).c_str());
      }
    }
    delay(100);
  }  

}
