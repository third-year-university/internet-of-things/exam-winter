//#include "pins_arduino.h"
#include <ESP8266WebServer.h>
#include "Sensor.h"


ESP8266WebServer server(80);

int sensor_to_use = 0;

void handle_root(){
  String form = "We use now: ";
  if (sensor_to_use == 1){
    form += "light sensor.";
  }
  else if (sensor_to_use == 0){
    form += "distance sensor.";
  }
  form += "<form action=\"/SENSOR\" method=\"POST\">";
  form += "<input type=\"submit\" value=\"Toogle sensor\">";
  form += "</form>";
  server.send(200, "text/html", form);
}

void handleSENSOR(){
  sensor_to_use = (sensor_to_use + 1) % 2;
  server.sendHeader("Location", "/");
  server.send(303);
}

void handleNotFound(){
  server.send(404, "text/html", "NOT FOUND!");
}
void server_init(){
  sensor_init();
  server.on("/", HTTP_GET, handle_root);
  server.on("/SENSOR", HTTP_POST, handleSENSOR);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("Server started at port 80");
}