int sensor_light_max_value = 0;

void init_light_sensor(){
  pinMode(A0, INPUT);
}

int get_light_sensor_value(){
  int value = analogRead(A0);
  sensor_light_max_value = max(sensor_light_max_value, value);
  return int(value * 1.0 / sensor_light_max_value * 255);
}
