#include <PubSubClient.h>
#define LED_PIN 15

PubSubClient mqtt_cli(network_client);

String mqtt_broker = "broker.emqx.io";
int mqtt_port = 1883;
String mqtt_client_id = "esp8266" + id();
char* data;

void callback(char* topic, byte* payload, unsigned int length){
  Serial.println("Message arrived on ");
  Serial.println(topic);
  int val = 0;
  for (int i = 0; i < length; i++){
      val *= 10;
      val += payload[i] - '0';
  }
  
  
  if (String(topic).equals("esp8266696D/sensor_light")){
    if (val < 128){
      digitalWrite(LED_PIN, HIGH);
    }
    else{
      digitalWrite(LED_PIN, LOW);
    }
  }
  if (String(topic).equals("esp8266696D/sensor_distance")){
    if (val < 20){
      Serial.println("LED ON");
      digitalWrite(LED_PIN, HIGH);
    }
    else{
      Serial.println("LED OFF");
      digitalWrite(LED_PIN, LOW);
    }
  }

  for (int i = 0; i < length; i++){
    Serial.print((char)payload[i]);
  }
  Serial.println();
  Serial.println("---------------");
}


void MQTT_init(){
  mqtt_cli.setServer(mqtt_broker.c_str(), mqtt_port);
  mqtt_cli.setCallback(callback);
  while(!mqtt_cli.connected()){
    Serial.print(mqtt_client_id);
    Serial.print(" connecting...");
    if (mqtt_cli.connect(mqtt_client_id.c_str())){
      Serial.println(" MQTT Connected!");
    }
    else{
      Serial.print("Failed to connect");
      Serial.println(mqtt_cli.state());
      delay(1000);
    }
  }
}