import random
import time

import paho.mqtt.client as mqtt_client

broker = "broker.emqx.io"

sensor1_value = 0
sensor2_value = 0

DISTANCE = 450
EPS = 20

def on_message(client, userdata, message):
    global sensor1_value
    global sensor2_value
    data = str(message.payload.decode("utf-8"))
    topic = message.topic
    #print(topic)
    if topic == 'esp8266696D/sensor':
        sensor1_value = int(data)
    elif topic == 'esp826689EC/sensor':
        sensor2_value = int(data)
    # print(sensor1_value + sensor2_value)
    # print(sensor1_value, sensor2_value)
    if DISTANCE*2 - EPS * 2 <= (sensor1_value + sensor2_value) <= DISTANCE*2 + EPS * 2:
        print("0 persons")
    elif DISTANCE - EPS <= (sensor1_value + sensor2_value) <= DISTANCE + EPS:
        print("1 person")
    else:
        print("2 and more persons")


client = mqtt_client.Client(f'isu_fbki_{random.randint(10000, 99999)}')
client.on_message = on_message

try:
    client.connect(broker)
except Exception:
    print('Failed to connect, check network')
    exit()

client.loop_start()

print('Subscribing')

client.subscribe("esp8266696D/sensor")
client.subscribe("esp826689EC/sensor")

time.sleep(600)
client.disconnect()
client.loop_stop()
print('Stop communication')
